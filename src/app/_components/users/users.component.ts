import { Component, OnInit } from '@angular/core';
import { UserService } from "../../_services/user.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  title: string;
  response: any = null;
  selectedUser: any;
  loading: boolean = false;
  total_pages: number;
  total_pages_array: number[] = [];
  current_page: number;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUserList(1);
  }

  showDetails(id: number) {
    this.loading = true;
    this.title = "User Detail";
    this.userService.getUsersById(id).subscribe(response => {
      this.selectedUser = response.data;      
      this.response = null;
      this.loading = false;
    });
  }

  goBack() {
    this.getUserList(1);
  }

  getUserList(page: number) {
    this.loading = true;
    this.title = "Users";
    this.userService.getUsersByPage(page).subscribe(response => {
      this.response = response;

      this.total_pages = this.response.total_pages;
      this.current_page = this.response.page;

      for (let index = 0; index < this.total_pages; index++) {
        this.total_pages_array[index] = index + 1;
      }
      this.selectedUser = null;
      this.loading = false;
    });
  }

  changePage(page: number) {
    this.getUserList(page);
  }

  nextPage() {
    if (this.current_page < this.total_pages) {
      this.current_page++;
      this.getUserList(this.current_page);
    }
  }

  prevPage() {
    if ((this.current_page > 1)) {
      this.current_page--;
      this.getUserList(this.current_page);
    }
  }

  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }
}
