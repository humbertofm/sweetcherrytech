import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from "../../_services/authentication.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  loading: boolean = false;
  response: any;

  constructor(private authenticationService: AuthenticationService, private router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      inputEmail: ['', Validators.required],
      inputPassword: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    } else {
      this.login();
    }
  }

  login() {
    this.loading = true;
    this.authenticationService.register(this.f.inputEmail.value, this.f.inputPassword.value).subscribe(response => {
      this.response = response;
      localStorage.setItem('token', this.response.token);
      console.log(localStorage.getItem('token'));
      this.router.navigate(['/colors'])
      this.loading = false;
    });
  }

}
