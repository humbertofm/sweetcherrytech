import { Component, OnInit } from '@angular/core';
import { ColorService } from "../../_services/color.service";

@Component({
  selector: 'app-colors',
  templateUrl: './colors.component.html',
  styleUrls: ['./colors.component.css']
})
export class ColorsComponent implements OnInit {
  title: string;
  response: any;
  selectedColor: any;
  loading: boolean = false;
  total_pages: number;
  total_pages_array: number[] = [];
  current_page: number;

  constructor(private colorService: ColorService) { }

  ngOnInit() {
    this.getColorList(1);
  }

  getColorList(page: number) {
    this.loading = true;
    this.title = "Colors";
    this.colorService.getColorsByPage(page).subscribe(response => {
      this.response = response;

      this.total_pages = this.response.total_pages;
      this.current_page = this.response.page;

      for (let index = 0; index < this.total_pages; index++) {
        this.total_pages_array[index] = index + 1;
      }
      this.selectedColor = null;
      this.loading = false;
    });
  }

  changePage(page: number) {
    this.getColorList(page);
  }

  nextPage() {
    if (this.current_page < this.total_pages) {
      this.current_page++;
      this.getColorList(this.current_page);
    }
  }

  prevPage() {
    if ((this.current_page > 1)) {
      this.current_page--;
      this.getColorList(this.current_page);
    }
  }

  showDetails(id: number) {
    this.loading = true;
    this.title = "Color Detail";
    this.colorService.getColorById(id).subscribe(response => {
      this.selectedColor = response.data;      
      this.response = null;
      this.loading = false;
    });
  }

  goBack() {
    this.getColorList(1);
  }

  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

}
