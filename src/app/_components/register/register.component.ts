import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from "../../_services/authentication.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  loading: boolean = false;
  response: any;

  constructor(private authenticationService: AuthenticationService, private router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      inputEmail: ['', Validators.required],
      inputPassword: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;


    if (this.f.inputPassword.errors) {
      console.log('contraseña');
      
    }

    if (this.f.inputEmail.errors) {
        console.log('email');
        
    }

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      console.log('sisa');
      
      return;
    } else {
      this.register();
    }
  }

  register() {
    this.loading = true;
    this.authenticationService.register(this.f.inputEmail.value, this.f.inputPassword.value).subscribe(response => {
      this.response = response;
      this.router.navigate(['/login'])
      this.loading = false;
    });
  }
}