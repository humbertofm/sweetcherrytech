import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sweetcherrytech';
  showMenu: boolean = false;
  activeColors: boolean = false;
  activeUsers: boolean = false;

  constructor(public router: Router) {
    router.events.subscribe(
      (response: any) => {
        // console.log(response.url); // print current url
        if (response.url != undefined && (response.url == "/login" || response.url == "/register" || response.url == "/")) {
          this.showMenu = false;
          this.activeColors = false;
          this.activeUsers = false;
        } else if (response.url != undefined && (response.url != "/login" || response.url != "/register" || response.url != "/")) {
          this.showMenu = true;
          this.activeColors = false;
          this.activeUsers = false;
        }
      }
    );
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['/login'])
  }

}
