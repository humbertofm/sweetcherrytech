import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './_components/login/login.component';
import { RegisterComponent } from './_components/register/register.component';
import { ColorsComponent } from './_components/colors/colors.component';
import { UsersComponent } from './_components/users/users.component';

import { UserService } from "./_services/user.service";
import { ColorService } from "./_services/color.service";
import { AuthenticationService } from "./_services/authentication.service";

import { routing } from "./app.routing";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ColorsComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    UserService,
    ColorService,
    AuthenticationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
