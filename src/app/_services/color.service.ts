import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ColorService {
  apiURL: string = 'https://reqres.in/api';

  constructor(private http: HttpClient) { }

  public getColorsByPage(page: number) {
    return this.http.get<any[]>(`${this.apiURL}/unknown?page=${page}&per_page=5`);
  }

  public getColorById(id: number) {
    return this.http.get<any>(`${this.apiURL}/unknown/${id}`);
  }

}