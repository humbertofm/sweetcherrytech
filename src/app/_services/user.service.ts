import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiURL: string = 'https://reqres.in/api';

  constructor(private http: HttpClient) { }

  public getUsersByPage(page: number) {
    return this.http.get<any[]>(`${this.apiURL}/users?page=${page}&per_page=5`);
  }

  public getUsersById(id: number) {
    return this.http.get<any>(`${this.apiURL}/users/${id}`);
  }
}
