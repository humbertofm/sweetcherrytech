import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  apiURL: string = 'https://reqres.in/api';
  

  constructor(private http: HttpClient) { }

  public register(email: string, password: string) {
    let body = {
      email: email,
      password: password
    }
    return this.http.post(`${this.apiURL}/register/`, body);
  }

  public login(email: string, password: string) {
    let body = {
      email: email,
      password: password
    }
    return this.http.post(`${this.apiURL}/login/`, body);
  }

}